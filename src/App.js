import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import Samuel from './components/Exemplo/Home';
import ProdutosPage from './components/Produtos/Produtos';

class App extends Component {

    render() {
        return(
            <div>
                <Switch>
                    <Route exact path="/" component={Samuel}/>
                    <Route exact path="/produtos" component={ProdutosPage}/>
                </Switch>
            </div>
        );
    }
}

export default App;
