import React, { Component } from 'react';
import ListarProdutos from './ListarProdutos';

class ProdutosPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            pastel: '123412341324',
            input_aqui: '',
        }

        this.handleChange = this.handleChange.bind(this);
    }

    trocarState() {
        const teste =' de BORBOLETAA';
        this.setState({
            pastel: `FLANGOOOOOOOOO ${teste}`
        });
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        })
    }

    render() {
        return (
            <div>
                <span>>>>>>>>>>>>>>{this.state.pastel}</span>
                <button onClick={ ()=> this.trocarState() }>CLIQUE AQUI</button>
                <div>
                    <input name="input_aqui" placeholder="Digite aqui" onChange={this.handleChange}/>
                </div>
                <h1>{this.state.input_aqui}</h1>

                <div>
                    <ListarProdutos/>
                </div>
            </div>
        );
    }
}

export default ProdutosPage;