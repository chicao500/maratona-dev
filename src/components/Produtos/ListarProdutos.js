import React, { Component } from 'react';
import Axios from 'axios';

class ListarProdutos extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            produtos: [],
        }
    }

    getProducts() {
        Axios({
            method: 'GET',
            url: 'http://10.64.192.205:3000/products',
        }).then((response)=> {
            this.setState({
                produtos: response.data
            })
            console.log(this.state.produtos);
        }).catch((error)=> {
            console.log(error);
        });
    }

    componentDidMount() {
        this.getProducts();
    }
    render() {
        return(
            <table>
                <thead>
                    <th>ID</th>
                    <th>Nome do Produto</th>
                    <th>Descrição do Produto</th>
                    <th>Preço do Produto</th>
                </thead>
                <tbody>
                    {this.state.produtos.map(produto => 
                        <tr>
                            <td>{produto.idProduto}</td>
                            <td>{produto.nomeProduto}</td>
                            <td>{produto.descricaoProduto}</td>
                            <td>{produto.precoProduto}</td>
                        </tr>    
                    )}
                </tbody>
            </table>
        );
    }
}

export default ListarProdutos;